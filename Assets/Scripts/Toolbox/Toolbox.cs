﻿using System;
using System.Collections.Generic;

namespace KOKO.ToolBox
{
    /// <summary>
    /// 1. Can be used as a simple script reference
    /// 2. Can be used as a GameContext that holds other SubContext and those having the actual references
    ///    See example code at the bottom.
    /// </summary>
    public static class Toolbox
    {
        private static Dictionary<Type, object> _tools = new Dictionary<Type, object>();

        /// <summary>
        /// Registers the Tool. Use on Awake only.
        /// </summary>
        /// <typeparam name="T">The tool's type.</typeparam>
        /// <param name="tool">The tool to register.</param>
        /// <returns>False if Tool of Type T already exists, true otherwise.</returns>
        public static bool RegisterTool<T>(T tool) where T : class
        {
            if (GetTool<T>() != null)
            {
                return false;
            }

            _tools.Add(typeof(T), tool);
            return true;
        }

        /// <summary>
        /// Gets the Tool of type T. Use on Start only.
        /// </summary>
        /// <typeparam name="T">The Type of the tool.</typeparam>
        /// <returns>Object of Type T found in Toolbox, otherwise default(T)</returns>
        public static T GetTool<T>() where T : class
        {
            object tool;

            if (_tools.TryGetValue(typeof(T), out tool))
            {
                return (T)tool;
            }

            return default(T);
        }

        /// <summary>
        /// Remove the tool from the toolbox
        /// </summary>
        /// <typeparam name="T">The Type of the tool.</typeparam>
        public static void DeregisterTool<T>(T tool) where T : class
        {
            _tools.Remove(typeof(T));
        }

        /* SubContext use: 
         * Context scripts execution order should always be set before normal time!
         * To use a reference you get the context from the toolbox and then get the specific reference property that you need:
         * 
         * IPlayerController _playerController = Toolbox.GetTool<PlayerContext>().PlayerController

        public class PlayerContext
        {
            [SerializeField]
            private PlayerController _playerController;

            public IPlayerController PlayerController { get { return _playerController; } }

            private void Awake()
            {
                RegisterOnAwake();
            }

            public void RegisterOnAwake()
            {
                GameContext.RegisterAsTool(this);
            }

            private void OnDestroy()
            {
                DeregisterOnDestroy();
            }

            public void DeregisterOnDestroy()
            {
                GameContext.DeregisterTool(this);
            }
        }

        */
    }
}