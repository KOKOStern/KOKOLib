﻿namespace KOKO.ToolBox
{
    // Useful as a reminder to implement registration and deregistration. Not actually needed.
    public interface ITool
    {
        void RegisterOnAwake();
        void DeregisterOnDestroy();
    }
}