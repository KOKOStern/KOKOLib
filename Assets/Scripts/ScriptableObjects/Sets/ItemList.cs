﻿using System.Collections.Generic;
using UnityEngine;

namespace KOKO.ScriptableObjects
{
    /* Use:
        [CreateAssetMenu]
        public class ThingRuntimeSet : RuntimeSet<Thing>
        {}
    */
    public abstract class ItemList<T> : ScriptableObject
    {
        [SerializeField]
        private List<T> _items = new List<T>();

        public void Add(T thing)
        {
            if (!_items.Contains(thing))
                _items.Add(thing);
        }

        public void Remove(T thing)
        {
            if (_items.Contains(thing))
                _items.Remove(thing);
        }
    }
}