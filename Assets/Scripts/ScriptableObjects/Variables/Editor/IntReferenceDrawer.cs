﻿using UnityEditor;
using UnityEngine;

namespace KOKO.ScriptableObjects
{
    [CustomPropertyDrawer(typeof(IntReference))]
    public class IntReferenceDrawer : PropertyDrawer
    {
        /// <summary>
        /// Options to display in the popup to select constant or variable.
        /// </summary>
        private readonly string[] R_PopupOptions = 
            { "Use Constant", "Use Variable" };

        /// <summary> Cached style to use to draw the popup button. </summary>
        private GUIStyle _popupStyle;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (_popupStyle == null)
            {
                _popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
                _popupStyle.imagePosition = ImagePosition.ImageOnly;
            }

            // Get properties
            SerializedProperty useConstant = property.FindPropertyRelative("_useConst");
            SerializedProperty constantValue = property.FindPropertyRelative("_constValue");
            SerializedProperty variable = property.FindPropertyRelative("_variable");

            if (!useConstant.boolValue && variable.objectReferenceValue != null)
            {
                label.text += " (" + ((IntVariable)variable.objectReferenceValue).Value.ToString() + ")";
            }

            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, label);
            
            EditorGUI.BeginChangeCheck();

            // Calculate rect for configuration button
            Rect buttonRect = new Rect(position);
            buttonRect.yMin += _popupStyle.margin.top;
            buttonRect.width = _popupStyle.fixedWidth + _popupStyle.margin.right;
            position.xMin = buttonRect.xMax;

            // Store old indent level and set it to 0, the PrefixLabel takes care of it
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            int result = EditorGUI.Popup(buttonRect, useConstant.boolValue ? 0 : 1, R_PopupOptions, _popupStyle);

            useConstant.boolValue = result == 0;

            EditorGUI.PropertyField(position, 
                useConstant.boolValue ? constantValue : variable, 
                GUIContent.none);

            if (EditorGUI.EndChangeCheck())
                property.serializedObject.ApplyModifiedProperties();

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();

            // This makes this script update ~10 times a second while selected.
            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }

    }
}