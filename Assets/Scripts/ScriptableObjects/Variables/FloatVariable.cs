﻿using UnityEngine;

namespace KOKO.ScriptableObjects
{
    [CreateAssetMenu(fileName = "FloatVar", menuName = "ScriptableObjects/Vars/FloatVar", order = 1)]
    public class FloatVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        [SerializeField]
        private string _description;
#endif
        [SerializeField]
        private float _value;

        public float Value { get { return _value; } }

        public void SetValue(float value)
        {
            _value = value;
        }

        public void SetValue(FloatVariable value)
        {
            _value = value._value;
        }

        public void ApplyChange(float amount)
        {
            _value += amount;
        }

        public void ApplyChange(FloatVariable amount)
        {
            _value += amount._value;
        }
    }
}