﻿using UnityEngine;

namespace KOKO.ScriptableObjects
{
    [CreateAssetMenu(fileName = "BoolVar", menuName = "ScriptableObjects/Vars/BoolVar", order = 3)]
    public class BoolVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        [SerializeField]
        private string _description;
#endif
        [SerializeField]
        private bool _value;

        public bool Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public void Toggle()
        {
            _value = !_value;
        }
    }
}