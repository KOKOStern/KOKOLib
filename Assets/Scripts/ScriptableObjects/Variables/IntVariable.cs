﻿using UnityEngine;

namespace KOKO.ScriptableObjects
{
    [CreateAssetMenu(fileName = "IntVar", menuName = "ScriptableObjects/Vars/IntVar", order = 0)]
    public class IntVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        [SerializeField]
        private string _description;
#endif
        [SerializeField]
        private int _value;

        public int Value { get { return _value; } }

        public void SetValue(int value)
        {
            _value = value;
        }

        public void SetValue(IntVariable value)
        {
            _value = value._value;
        }

        public void ApplyChange(int amount)
        {
            _value += amount;
        }

        public void ApplyChange(IntVariable amount)
        {
            _value += amount._value;
        }
    }
}
