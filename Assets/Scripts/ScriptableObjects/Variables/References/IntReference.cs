﻿using System;
using UnityEngine;

namespace KOKO.ScriptableObjects
{
    /// <summary>
    /// Only used as a reference
    /// </summary>
    [Serializable]
    public class IntReference
    {
        [SerializeField]
        private bool _useConst = true;
        [SerializeField]
        private int _constValue;
        [SerializeField]
        private IntVariable _variable;

        public int Value
        {
            get { return _useConst ? _constValue : _variable.Value; }
        }

        // Can be used as a float.
        public static implicit operator int(IntReference reference)
        {
            return reference.Value;
        }
    }
}