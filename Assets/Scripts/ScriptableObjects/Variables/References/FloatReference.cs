﻿using System;
using UnityEngine;

namespace KOKO.ScriptableObjects
{
    /// <summary>
    /// Only used as a reference, can't change the value.
    /// </summary>
    [Serializable]
    public class FloatReference
    {
        [SerializeField]
        private bool _useConst = true;
        [SerializeField]
        private float _constValue;
        [SerializeField]
        private FloatVariable _variable;

        public float Value
        {
            get { return _useConst ? _constValue : _variable.Value; }
        }
    }
}