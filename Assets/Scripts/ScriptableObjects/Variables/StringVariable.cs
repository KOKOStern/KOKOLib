﻿using UnityEngine;

namespace KOKO.ScriptableObjects
{
    [CreateAssetMenu(fileName = "StringVar", menuName = "ScriptableObjects/Vars/StringVar", order = 2)]
    public class StringVariable : ScriptableObject
    {
        [SerializeField]
        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}