﻿using UnityEditor;
using UnityEngine;

namespace KOKO.ScriptableObjects
{
    [CustomEditor(typeof(SpriteContainer))]
    [CanEditMultipleObjects]
    public class SpriteContainerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            SpriteContainer spriteContainer = target as SpriteContainer;
            
            if (spriteContainer.IsOverrideSorting)
            {
                string[] sortingLayerNames = SortingLayerNames();
                int sortingLayerIndex = EditorGUILayout.Popup("Sorting Layer", System.Array.IndexOf(sortingLayerNames, spriteContainer.SortingLayerName), sortingLayerNames);
                spriteContainer.SortingLayerName = sortingLayerNames[sortingLayerIndex];
                spriteContainer.OrderInLayer = EditorGUILayout.IntField("Order in Layer", spriteContainer.OrderInLayer);
            }

            EditorUtility.SetDirty(target);
        }

        private static string[] SortingLayerNames()
        {
            string[] sortingLayerNames = new string[SortingLayer.layers.Length];
            for(int i = 0; i < sortingLayerNames.Length; i++)
            {
                sortingLayerNames[i] = SortingLayer.layers[i].name;
            }
            return sortingLayerNames;
        }
    }
}
