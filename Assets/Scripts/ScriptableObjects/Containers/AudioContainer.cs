﻿using UnityEngine;
using UnityEngine.Audio;

namespace KOKO.ScriptableObjects
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Containers/Audio Container")]
    public class AudioContainer : ScriptableObject
    {
        [SerializeField]
        private AudioClip _audioClip;
        [SerializeField]
        private AudioMixerGroup _output;
        [SerializeField]
        private bool _loop;
        [SerializeField]
        [Range(0, 1)]
        private float _volume = 1;

        public AudioClip GetAudioClip { get { return _audioClip; } }

        public void Initialize(AudioClip audioClip)
        {
            _audioClip = audioClip;
        }

        public void ApplyToAudioSource(AudioSource audioSource)
        {
            if(audioSource == null)
            {
                return;
            }
            audioSource.clip = _audioClip;
            audioSource.outputAudioMixerGroup = _output;
            audioSource.loop = _loop;
            audioSource.volume = _volume;
        }
    }
}
