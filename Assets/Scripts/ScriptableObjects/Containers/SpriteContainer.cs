﻿using UnityEngine;

namespace KOKO.ScriptableObjects
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Containers/Sprite Container")]
    public class SpriteContainer : ScriptableObject
    {
        [SerializeField]
        private Sprite _sprite;
        [SerializeField]
        private bool _overrideSorting = false;
        [SerializeField]
        [HideInInspector]
        private string _sortingLayerName = "Default";
        [SerializeField]
        [HideInInspector]
        private int _orderInLayer = 0;

        public bool IsOverrideSorting { get { return _overrideSorting; } }
        public string SortingLayerName { get { return _sortingLayerName; } set { _sortingLayerName = value; } }
        public int OrderInLayer { get { return _orderInLayer; } set { _orderInLayer = value; } }
        
        public void Initialize(Sprite sprite)
        {
            _sprite = sprite;
        }

        public Sprite GetSprite { get { return _sprite; } }

        public void ApplyToSpriteRenderer(SpriteRenderer spriteRenderer)
        {
            if(spriteRenderer == null)
            {
                return;
            }
            spriteRenderer.sprite = _sprite;
            if (_overrideSorting)
            {
                spriteRenderer.sortingLayerName = _sortingLayerName;
                spriteRenderer.sortingOrder = _orderInLayer;
            }
        }
    }
}
