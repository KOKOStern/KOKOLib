﻿using UnityEngine;
using UnityEngine.Events;

namespace KOKO.ScriptableObjects
{
    public class GameEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField]
        private GameEvent _event;

        [Tooltip("Response to invoke when Event is raised.")]
        private UnityEvent _response;

        private void OnEnable()
        {
            _event.RegisterListener(this);
        }

        private void OnDisable()
        {
            _event.UnregisterListener(this);
        }

        public void OnEventRaised()
        {
            _response.Invoke();
        }

        public void SetResponse(UnityEvent response)
        {
            _response = response;
        }
    }
}