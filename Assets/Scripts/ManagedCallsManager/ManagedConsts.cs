﻿namespace KOKO.ManagedCalls
{
	public static class ManagedConsts 
	{
        #region Default Order

        public const int O_DefaultNegTen = -10;

        public const int O_DefaultZero = 0;

        public const int O_DefaultTen = 10;

        #endregion Default Order

        #region Unique Scripts

        #endregion Unique Scripts
    }
}
