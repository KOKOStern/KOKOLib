﻿using KOKO.EditorUtils;
using System.Collections.Generic;
using UnityEngine;

namespace KOKO.ManagedCalls
{
    /// <summary>
    /// This manager class holds ordered arrays of core calls
    /// Init - will happen on Start time
    /// Update / FixedUpdate / LateUpdate - will happen on appropriate Update time
    ///
    /// Each call needs to be registered to on Awake (before registration closes on Start time).
    /// On Start all arrays will be created and sorted.
    /// 
    /// TODO
    /// - Consider adding an 'active' bool to each call to either call it or not -
    /// ---- DECIDED - burden should be on script not manager
    /// - Consider adding a 'timed' managed call that will run on update every some time.
    /// - Consider a separate (???) linked-list solution that can take care of 'live' registration
    ///   LiveManagedCalls. 
    ///   Allow removal from the list if needed (or just inactive?)
    ///   Check if this is faster than just reordering the list as we do now.
    /// 
    /// USE
    /// Inherit and place in scene.
    /// Other monobehaviours should implement an IManaged interface and then get this or IManagedCallsManager and register.
    /// </summary>
    [ScriptOrder(-100)]
    public abstract class ManagedCallsManager : MonoBehaviour, IManagedCallsManager
    {
        private bool _initialRegistrationOpen;
        private List<IManagedInit> _managedInitList;
        private List<IManagedUpdate> _managedUpdateList;
        private List<IManagedFixedUpdate> _managedFixedUpdateList;
        private List<IManagedLateUpdate> _managedLateUpdateList;

        private IManagedInit[] _managedInits;
        private IManagedUpdate[] _managedUpdates;
        private IManagedFixedUpdate[] _managedFixedUpdates;
        private IManagedLateUpdate[] _managedLateUpdates;

        private IComparer<IManagedInit> _managedInitComparer;
        private IComparer<IManagedUpdate> _managedUpdateComparer;
        private IComparer<IManagedFixedUpdate> _managedFixedUpdateComparer;
        private IComparer<IManagedLateUpdate> _managedLateUpdateComparer;

        /// <summary>
        /// Can be overriden for any other awake tasks, but should call base for EnableRegistration and InitLists.
        /// </summary>
        protected virtual void Awake()
        {
            EnableRegistration();
            InitLists();
            InitComparers();
        }

        protected virtual void EnableRegistration()
        {
            _initialRegistrationOpen = true;
        }

        protected virtual void DisableRegistration()
        {
            _initialRegistrationOpen = false;
        }

        protected virtual void InitLists()
        {
            _managedInitList = new List<IManagedInit>();
            _managedFixedUpdateList = new List<IManagedFixedUpdate>();
            _managedUpdateList = new List<IManagedUpdate>();
            _managedLateUpdateList = new List<IManagedLateUpdate>();
        }

        protected void InitComparers()
        {
            _managedInitComparer = new ManagedInitComparer();
            _managedUpdateComparer = new ManagedUpdateComprarer();
            _managedFixedUpdateComparer = new ManagedFixedUpdateComparer();
            _managedLateUpdateComparer = new ManagedLateUpdateComparer();
        }


        #region Managed Calls Manager Registration

        public void Register(MonoBehaviour script)
        {

            if (_initialRegistrationOpen)
            {
                if (script is IManagedInit)
                {
                    _managedInitList.Add(script as IManagedInit);
                }
                if (script is IManagedUpdate)
                {
                    _managedUpdateList.Add(script as IManagedUpdate);
                }
                if (script is IManagedFixedUpdate)
                {
                    _managedFixedUpdateList.Add(script as IManagedFixedUpdate);
                }
                if (script is IManagedLateUpdate)
                {
                    _managedLateUpdateList.Add(script as IManagedLateUpdate);
                }
            }
            else
            {
                if (script is IManagedInit)
                {
                    (script as IManagedInit).ManagedInit();
                }
                if (script is IManagedUpdate)
                {
                    _managedUpdateList.Add(script as IManagedUpdate);
                    _managedUpdates = _managedUpdateList.ToArray();
                    System.Array.Sort(_managedUpdates, _managedUpdateComparer);
                }
                if (script is IManagedFixedUpdate)
                {
                    _managedFixedUpdateList.Add(script as IManagedFixedUpdate);
                    _managedFixedUpdates = _managedFixedUpdateList.ToArray();
                    System.Array.Sort(_managedFixedUpdates, _managedFixedUpdateComparer);
                }
                if (script is IManagedLateUpdate)
                {
                    _managedLateUpdateList.Add(script as IManagedLateUpdate);
                    _managedLateUpdates = _managedLateUpdateList.ToArray();
                    System.Array.Sort(_managedLateUpdates, _managedLateUpdateComparer);
                }
            }
        }

        #endregion Managed Calls Manager Registration

        #region ManagedCalls

        private void Start()
        {
            DisableRegistration();

            CreateManagedArrays();
            SortManagedArrays();

            ManagedInitCall();
        }

        private void CreateManagedArrays()
        {
            _managedInits = _managedInitList.ToArray();
            _managedUpdates = _managedUpdateList.ToArray();
            _managedFixedUpdates = _managedFixedUpdateList.ToArray();
            _managedLateUpdates = _managedLateUpdateList.ToArray();
        }

        private void SortManagedArrays()
        {
            System.Array.Sort(_managedInits, _managedInitComparer);
            System.Array.Sort(_managedUpdates, _managedUpdateComparer);
            System.Array.Sort(_managedFixedUpdates, _managedFixedUpdateComparer);
            System.Array.Sort(_managedLateUpdates, _managedLateUpdateComparer);
        }

        private void ManagedInitCall()
        {
            for (int i = 0; i < _managedInits.Length; i++)
            {
                _managedInits[i].ManagedInit();
            }
        }

        private void Update()
        {
            for (int i = 0; i < _managedUpdates.Length; i++)
            {
                _managedUpdates[i].ManagedUpdate();
            }
        }

        private void FixedUpdate()
        {
            for (int i = 0; i < _managedFixedUpdates.Length; i++)
            {
                _managedFixedUpdates[i].ManagedFixedUpdate();
            }
        }

        private void LateUpdate()
        {
            for (int i = 0; i < _managedLateUpdates.Length; i++)
            {
                _managedLateUpdates[i].ManagedLateUpdate();
            }
        }

        #endregion ManagedCalls
    }
}