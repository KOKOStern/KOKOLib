﻿using UnityEngine;

namespace KOKO.ManagedCalls
{
    public interface IManagedCallsManager
    {
        void Register(MonoBehaviour script);
    }
}