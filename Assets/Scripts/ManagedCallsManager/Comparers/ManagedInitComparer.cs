﻿using System.Collections.Generic;

namespace KOKO.ManagedCalls
{
	public class ManagedInitComparer : IComparer<IManagedInit>
    {
        public int Compare(IManagedInit x, IManagedInit y)
        {
            if (x.GetInitOrder == y.GetInitOrder)
            {
                return 0;
            }
            if (x.GetInitOrder > y.GetInitOrder)
            {
                return 1;
            }
            return -1;
        }
    }
}
