﻿using System.Collections.Generic;

namespace KOKO.ManagedCalls
{
	public class ManagedLateUpdateComparer : IComparer<IManagedLateUpdate>
    {
        public int Compare(IManagedLateUpdate x, IManagedLateUpdate y)
        {
            if (x.GetLateUpdateOrder == y.GetLateUpdateOrder)
            {
                return 0;
            }
            if (x.GetLateUpdateOrder > y.GetLateUpdateOrder)
            {
                return 1;
            }
            return -1;
        }
    }
}
