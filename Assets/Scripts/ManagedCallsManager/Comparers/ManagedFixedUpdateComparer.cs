﻿using System.Collections.Generic;

namespace KOKO.ManagedCalls
{
	public class ManagedFixedUpdateComparer : IComparer<IManagedFixedUpdate>
    {
        public int Compare(IManagedFixedUpdate x, IManagedFixedUpdate y)
        {
            if (x.GetFixedUpdateOrder == y.GetFixedUpdateOrder)
            {
                return 0;
            }
            if (x.GetFixedUpdateOrder > y.GetFixedUpdateOrder)
            {
                return 1;
            }
            return -1;
        }
    }
}
