﻿using System.Collections.Generic;

namespace KOKO.ManagedCalls
{
    public class ManagedUpdateComprarer : IComparer<IManagedUpdate>
    {
        public int Compare(IManagedUpdate x, IManagedUpdate y)
        {
            if (x.GetUpdateOrder == y.GetUpdateOrder)
            {
                return 0;
            }
            if (x.GetUpdateOrder > y.GetUpdateOrder)
            {
                return 1;
            }
            return -1;
        }
    }
}
