﻿namespace KOKO.ManagedCalls
{
	public interface IManagedInit
    {
        void ManagedInit();
        int GetInitOrder { get; }
    }
}
