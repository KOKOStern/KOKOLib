﻿namespace KOKO.ManagedCalls
{
	public interface IManagedLateUpdate
	{
        void ManagedLateUpdate();
        int GetLateUpdateOrder { get; }
    }
}
