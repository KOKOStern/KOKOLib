﻿namespace KOKO.ManagedCalls
{
    /// <summary>
    /// Asks for update to be called ever frame, in order of GetOrder in relation to all other updates.
    /// </summary>
	public interface IManagedUpdate
	{
        void ManagedUpdate();
        int GetUpdateOrder { get; }
    }
}
