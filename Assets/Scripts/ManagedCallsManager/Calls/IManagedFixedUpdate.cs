﻿namespace KOKO.ManagedCalls
{
	public interface IManagedFixedUpdate
	{
        void ManagedFixedUpdate();
        int GetFixedUpdateOrder { get; }
    }
}
