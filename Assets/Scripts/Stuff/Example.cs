﻿using KOKO.ScriptableObjects;
using UnityEngine;

public class Example : MonoBehaviour 
{
    public IntVariable intVar;
    public IntReference intRef;

    public FloatVariable floatVar;
    public FloatReference floatRef;

    public BoolVariable boola;
}
