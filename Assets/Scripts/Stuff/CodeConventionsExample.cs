﻿// This is not complete!
//-----------

// Only keep using statements that are actually needed.
using UnityEngine;

namespace NameSpace.Structure
{
    /// <summary>
    /// What does this class do? This should be as short and concise as possible. One sentence so I get the class.
    /// Generally code should be 'self-documenting'.
    /// If the layout and naming conventions are good enough - you should understand the code without comments.
    /// </summary>
    public class CodeConventionsExample : MonoBehaviour
    {
        // Do not use public vars just to show things in the inspector
        // Always use explicit accessors
        // Private vars use an underscore at the start
        // PascalCase
        // Do not initialize serialized fields (there's no point)
        [Header("Use headers to group logical units in the inspector")]
        // Attributes and property drawers are your friend
        [Range(1, 10)]
        [SerializeField]
        // Don't be afraid of giant names if they explain what the var does
        // Type of value type should be clear from the name
        private int _thisNumberBetweenOneAndTen;

        protected int _stillUnderscore;
        public int DoYouReallyNeedPublic;

        private bool _isThisAQuestion;

        // Type of ref types should be the last word of the name.
        private Transform _thisIsTheTransform;

        // Do not leave empty unity functions.
        // Brackets open like C# not like Java.
        // Indent is 4 spaces.
        private void Awake()
        {
            // Cache any component you use more than once.
            _thisIsTheTransform = GetComponent<Transform>();
        }

        #region RegionName

        // Make sure regions have names are the end of them as well
        // Use regions if the class is too big - over 150 lines / ~2 screen of code

        public void ConditionsYo()
        {
            if (_isThisAQuestion)
            {
                // Brackets even though we have only one command in here.
                _thisIsTheTransform.position = Vector3.zero;
            }
        }

        #endregion RegionName
    }
}