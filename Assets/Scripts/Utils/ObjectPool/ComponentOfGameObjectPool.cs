﻿using UnityEngine;
using System.Collections.Generic;

namespace KOKO.Utils
{
    /// <summary>
    /// Generic gameobject pool that instantiates new objects when needed.
    /// The pool assumes the gameobject has class T as a component. If not, will return default (null)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ComponentOfGameObjectPool<T> where T:Component
    {
        private GameObject _objectOriginal;
        private List<T> _pool;

        public ComponentOfGameObjectPool(GameObject original)
        {
            _objectOriginal = original;
            _pool = new List<T>();
        }

        public T GetObject(Vector3 position, Quaternion rotation, Transform parent)
        {
            if (_pool.Count > 0)
            {
                // We remove the last because RemoveAt is an O(Count - index) operation. This means removing the last is best.
                T obj = _pool[_pool.Count - 1];
                _pool.RemoveAt(_pool.Count - 1);

                // Apply transform params;
                Transform transform = obj.transform;
                transform.position = position;
                transform.rotation = rotation;
                transform.SetParent(parent, false);
                return obj;
            }
            else
            {
                return CreateNewObject(position, rotation, parent);
            }
        }

        public T GetObject()
        {
            return GetObject(Vector3.zero, Quaternion.identity, null);  
        }

        // More efficient to instantiate with transform data
        private T CreateNewObject(Vector3 position, Quaternion rotation, Transform parent)
        {
            GameObject newObject = Object.Instantiate(_objectOriginal, position, rotation, parent);
            T newComponent = newObject.GetComponent<T>();
            if (ReferenceEquals(newComponent, null))
            {
                Object.Destroy(newObject);
                return default(T);
            }
            return newComponent;
        }

        public void ReturnToPool(T returnObject)
        {
            _pool.Add(returnObject);
        }
		
		public void TrimPoolToSize(int maxSize)
        {
            // Ensure this operation makes sense.
            if (maxSize < _pool.Count)
            {
                for (int i = _pool.Count - 1; i >= maxSize; i--)
                {
                    T Obj = _pool[i];
                    Object.Destroy(Obj.gameObject);
                }
                _pool.RemoveRange(maxSize, _pool.Count - maxSize);
            }
        }
    }
}