﻿using UnityEngine;
using UnityEditor;

namespace KOKO.EditorUtils
{
	public static class CreateScriptableObjectAsset 
	{
		/* Use:
		[MenuItem("Assets/Create/InserNameHere")]
        public static void CreateScriptableObjectName()
        {
            CreateScriptableObjectAsset.CreateAsset<ScriptableObjectName>();
        }*/
		
        public static void CreateAsset<T>() where T : ScriptableObject
        {
            CreateAsset<T>(AssetDatabase.GetAssetPath(Selection.activeObject));
        }

        public static T CreateAsset<T>(string specificPath) where T : ScriptableObject
        {
            // Make the asset
            T asset = ScriptableObject.CreateInstance<T>();

            // Get the path.
            string path = specificPath;

            // If it's empty, we put it at root.
            if (path == string.Empty)
            {
                path = "Assets";
            }
            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(T).ToString() + ".asset");

            try
            {
                AssetDatabase.CreateAsset(asset, assetPathAndName);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = asset;
                return asset;
            }
            catch (System.Exception e)
            {
                Debug.LogErrorFormat("Could not create asset at path: {0}. Error follows:", path);
                Debug.LogErrorFormat("{0}", e.Message);
                return null;
            }
        }

        public static ScriptableObject DuplicateAsset(ScriptableObject objectToDup)
        {
            string path = AssetDatabase.GetAssetPath(objectToDup);
            string newPath = AssetDatabase.GenerateUniqueAssetPath(path).Replace(".asset", " COPY.asset");

            try
            {
                AssetDatabase.CopyAsset(path, newPath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                EditorUtility.FocusProjectWindow();
                ScriptableObject newObj = AssetDatabase.LoadAssetAtPath(newPath, typeof(ScriptableObject)) as ScriptableObject;
                Selection.activeObject = newObj;
                return newObj;
            }
            catch (System.Exception e)
            {
                Debug.LogFormat("Couldn't duplicate object!");
                Debug.LogErrorFormat("{0}", e.Message);
                return null;
            }
        }
    }
}