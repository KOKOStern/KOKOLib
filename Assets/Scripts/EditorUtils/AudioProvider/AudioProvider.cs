﻿using UnityEngine;

namespace KOKO.Audio
{
    // This is a very simple container for an audio clip.
    public class AudioProvider : ScriptableObject
    {
        public AudioClip audioClip;
        public bool bypassEffects;
        public bool bypassListenerEffects;
        public bool bypassReverbZones;
        public bool loop;

        [Range(0, 256)]
        public int priority = 128;
        [Range(0, 1)]
        public float volume = 1;
        [Range(-3, 3)]
        public float pitch = 1;
        [Range(-1, 1)]
        public float stereoPan = 0;
        [Range(0, 1)]
        public float spatialBlend = 1;
        [Range(0, 1.1f)]
        public float reverbZoneMix = 1;
        [Header("3D Sound Settings")]
        [Range(0, 5)]
        public float dopplerLevel = 1;
        [Range(0, 360)]
        public float spread = 10;
        public AudioRolloffMode volumeRolloff = AudioRolloffMode.Logarithmic;
        public float minDistance = 1;
        public float maxDistance = 500;

        public static void SetupAudioSourceWithProvider(AudioSource audioSource, AudioProvider audioProvider)
        {
            audioSource.clip = audioProvider.audioClip;
            audioSource.bypassEffects = audioProvider.bypassEffects;
            audioSource.bypassListenerEffects = audioProvider.bypassListenerEffects;
            audioSource.bypassReverbZones = audioProvider.bypassReverbZones;
            audioSource.loop = audioSource.loop;
            audioSource.priority = audioProvider.priority;
            audioSource.volume = audioProvider.volume;
            audioSource.pitch = audioProvider.pitch;
            audioSource.panStereo = audioProvider.stereoPan;
            audioSource.spatialBlend = audioProvider.spatialBlend;
            audioSource.reverbZoneMix = audioProvider.reverbZoneMix;
            audioSource.dopplerLevel = audioProvider.dopplerLevel;
            audioSource.spread = audioProvider.spread;
            audioSource.rolloffMode = audioProvider.volumeRolloff;
            audioSource.minDistance = audioProvider.minDistance;
            audioSource.maxDistance = audioProvider.maxDistance;
        }
    }
}