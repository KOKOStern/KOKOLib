﻿using KOKO.EditorUtils;
using UnityEditor;

namespace KOKO.Audio
{
    [CustomEditor(typeof(AudioProvider))]
    public class AudioProviderEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (((AudioProvider)target).audioClip != null)
            {
                EditorGUILayout.LabelField("Clip Length: ", ((AudioProvider)target).audioClip.length.ToString());
            }
            else
            {
                EditorGUILayout.LabelField("Clip Length: ", "0");
            }
            DrawDefaultInspector();
        }

        [MenuItem("Assets/Create/AudioProvider")]
        public static void CreateAudioProvider()
        {
            CreateScriptableObjectAsset.CreateAsset<AudioProvider>();
        }
    }
}