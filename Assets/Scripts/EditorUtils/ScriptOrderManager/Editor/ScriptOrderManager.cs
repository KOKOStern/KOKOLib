﻿using System;
using UnityEditor;

namespace KOKO.EditorUtils
{
    [InitializeOnLoad]
    public class ScriptOrderManager
    {
        static ScriptOrderManager()
        {
            foreach (MonoScript monoScript in MonoImporter.GetAllRuntimeMonoScripts())
            {
                if (monoScript.GetClass() != null)
                {
                    Attribute a = Attribute.GetCustomAttribute(monoScript.GetClass(), typeof(ScriptOrder));

                    if (!ReferenceEquals(a, null))
                    {
                        int currentOrder = MonoImporter.GetExecutionOrder(monoScript);
                        int newOrder = ((ScriptOrder)a).order;
                        if (currentOrder != newOrder)
                        {
                            UnityEngine.Debug.LogFormat("The following error is an internal Unity bug caused during the process " +
                                                        "of setting the ScriptOrder attribute.\nThe process is successful despite the error, please ignore." +
                                                        "\n If you see no error - can remove this Debug Log (Script Order Manager)");
                            MonoImporter.SetExecutionOrder(monoScript, newOrder);
                        }
                    }
                }
            }
        }
    }
}
