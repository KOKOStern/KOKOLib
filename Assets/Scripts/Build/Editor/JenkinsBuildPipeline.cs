﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace KOKO.Build
{
    // Run in CMD:
    // unity.exe -quit -batchmode -executeMethod KOKO.Build.JenkinsBuildPipeline.PerformBuild
    // Application.isBatchMode to check if we're running batch and not manual builds.
    public class JenkinsBuildPipeline
    {
        public static void PerformBuild()
        {
            Debug.LogFormat("Building time!");

            BuildPlayerOptions options = new BuildPlayerOptions();
            options.scenes = FindEnabledEditorScenes();
            options.locationPathName = SetupLocationPathName();
            options.options = BuildOptions.None;
            options.target = BuildTarget.WebGL;
            options.targetGroup = BuildTargetGroup.WebGL;

            BuildReport report = BuildPipeline.BuildPlayer(options);
            // This is with Addressables
            //BuildReport report = BuildPlayer(options);

            // Show this somehow?
            //report.ToString();
        }

        /*
        private static BuildReport BuildPlayer(BuildPlayerOptions options)
        {
            AddressableAssetSettings settings = AddressableAssetSettingsDefaultObject.Settings;
            if (settings == null)
            {
                if (BuildScript.buildCompleted != null)
                {
                    BuildScript.buildCompleted(new AddressableAssetBuildResult() { Duration = 0, Error = "AddressableAssetSettings not found." });
                }
                return BuildPipeline.BuildPlayer(options);
            }
            else
            {
                if (settings.ActivePlayerDataBuilder == null)
                {
                    string err = "Active build script is null.";
                    Debug.LogError(err);

                    if (BuildScript.buildCompleted != null)
                    {
                        BuildScript.buildCompleted(new AddressableAssetBuildResult() { Duration = 0, Error = err });
                    }
                    return null;
                }

                if (!settings.ActivePlayerDataBuilder.CanBuildData<AddressablesPlayerBuildResult>())
                {
                    string err = string.Format("Active build script {0} cannot build AddressablesPlayerBuildResult.", settings.ActivePlayerDataBuilder);
                    Debug.LogError(err);

                    if (BuildScript.buildCompleted != null)
                    {
                        BuildScript.buildCompleted(new AddressableAssetBuildResult() { Duration = 0, Error = err });
                    }
                    return null;
                }

                AddressablesBuildDataBuilderContext context = new AddressablesBuildDataBuilderContext(settings, 
                    options.targetGroup, 
                    options.target, 
                    (options.options & BuildOptions.Development) != BuildOptions.None, 
                    ((options.options & BuildOptions.ConnectWithProfiler) != BuildOptions.None), 
                    settings.PlayerBuildVersion);
                AddressablesPlayerBuildResult res = settings.ActivePlayerDataBuilder.BuildData<AddressablesPlayerBuildResult>(context);
                BuildReport report = BuildPipeline.BuildPlayer(options);

                if (!string.IsNullOrEmpty(res.ContentStateDataPath))
                {
                    string newPath = ContentUpdateScript.GetContentStateDataPath(false);
                    if (File.Exists(newPath))
                    {
                        File.Delete(newPath);
                    }
                    File.Copy(res.ContentStateDataPath, newPath);
                }

                if (BuildScript.buildCompleted != null)
                {
                    BuildScript.buildCompleted(res);
                }

                return report;
            }
        }*/
        
        private static string[] FindEnabledEditorScenes()
        {
            List<string> EditorScenes = new List<string>();
            foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
            {
                if (scene.enabled)
                {
                    EditorScenes.Add(scene.path);
                }
            }
            return EditorScenes.ToArray();
        }

        private static string SetupLocationPathName()
        {
            string directoryPath = Directory.GetParent(Application.dataPath) + "/Builds/Game_" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm") + "/";

            Debug.LogFormat("Dir Path: {0}", directoryPath);

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            //string exePath = "Game_" + System.DateTime.Now.ToString("dd/MM/yy HH:mm") + ".exe";

            return directoryPath;
        }
    }
}