﻿using TMPro;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace KOKO.Build
{
    /// <summary>
    /// This will look for VersionText every time a scene is built, which is when you press play.
    /// The build check will ensure this only happens when we actually build the game.
    /// </summary>
    public class PreBuildVersioning
    {
        [PostProcessScene]
        public static void ChangeVersionTextToNow()
        {
            if (!BuildPipeline.isBuildingPlayer)
            {
                return;
            }

            Debug.LogFormat("PreBuild: Looking for VersionText Object");
            GameObject versionText = GameObject.Find("VersionText");
            if (!ReferenceEquals(versionText, null))
            {
                Debug.LogFormat("PreBuild: Looking for UGUI");
                TextMeshProUGUI textUGUI = versionText.GetComponent<TextMeshProUGUI>();
                if (!ReferenceEquals(textUGUI, null))
                {
                    Debug.LogFormat("PreBuild: Updating version!");
                    textUGUI.text = "v" + System.DateTime.Now.ToString("dd/MM/yy HH:mm");
                    // We don't call this to not save the scene so it won't actually change.
                    //EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
                    return;
                }
            }
            Debug.LogWarningFormat("PreBuild: Could not find VersionText Gameobject with TextMeshProUGUI Component to write version.");
        }
    }
}

